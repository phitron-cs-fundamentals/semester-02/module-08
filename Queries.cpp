#include <bits/stdc++.h>
using namespace std;
class Node
{
public:
    int val;
    Node *next;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
    }
};

void insert_head(Node *&head, int v)
{
    Node *newNode = new Node(v);
    newNode->next = head;
    head = newNode;
}

void insert_tail(Node *&head, int v)
{
    Node *newNode = new Node(v);
    if (head == NULL)
    {
        head = newNode;
        return;
    }
    Node *tmp = head;
    while (tmp->next != NULL)
    {
        tmp = tmp->next;
    }
    tmp->next = newNode;
}

void print_linked_list(Node *head)
{
    Node *tmp = head;
    while (tmp != NULL)
    {
        cout << tmp->val << " ";
        tmp = tmp->next;
    }
    cout << endl;
}

void delete_index(Node *&head, int index)
{
    Node *tmp = head;
    if (head == NULL)
        return;

    if (index == 0)
    {
        head = head->next;
        delete tmp;
        return;
    }

    for (int i = 0; tmp != NULL && i < index - 1; i++)
    {
        tmp = tmp->next;
    }

    if (tmp == NULL || tmp->next == NULL)
        return;

    Node *deleteNode = tmp->next;
    tmp->next = tmp->next->next;
    delete deleteNode;
}

int main()
{
    Node *head = NULL;
    int q;
    cin >> q;
    while (q--)
    {
        int X, V;
        cin >> X >> V;

        if (X == 0)
        {
            insert_head(head, V);
        }
        else if (X == 1)
        {
            insert_tail(head, V);
        }
        else if (X == 2)
        {
            delete_index(head, V);
        }
        print_linked_list(head);
    }
    return 0;
}
