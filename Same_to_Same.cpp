#include <bits/stdc++.h>

//partial marks cut

using namespace std;
class Node
{
public:
    int val;
    Node *next;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
    }
};
void insert_tail_1(Node *&head1, Node *&tail1, int val1)
{
    Node *newNode = new Node(val1);
    if (head1 == NULL)
    {
        head1 = newNode;
        tail1 = newNode;
        return;
    }
    else
    {
        tail1->next = newNode;
        tail1 = newNode;
    }
}

void insert_tail_2(Node *&head2, Node *&tail2, int val2)
{
    Node *newNode = new Node(val2);
    if (head2 == NULL)
    {
        head2 = newNode;
        tail2 = newNode;
        return;
    }
    else
    {
        tail2->next = newNode;
        tail2 = newNode;
    }
}

bool node_equal(Node *head1, Node *head2)
{
    Node *tmp1 = head1;
    Node *tmp2 = head2;
    while (tmp1 != NULL && tmp2 != NULL)
    {
        if (head1->val != head2->val)
        {
            return false;
        }
        tmp1 = tmp1->next;
        tmp2 = tmp2->next;
    }
    return (tmp1 == NULL && tmp2 == NULL);
}

int size_1(Node *head1)
{
    Node *tmp1 = head1;
    int count = 0;
    while (tmp1 != NULL)
    {
        count++;
        tmp1 = tmp1->next;
    }
    return count;
}
int size_2(Node *head2)
{
    Node *tmp2 = head2;
    int count = 0;
    while (tmp2 != NULL)
    {
        count++;
        tmp2 = tmp2->next;
    }
    return count;
}

int main()
{
    Node *head1 = NULL;
    Node *tail1 = NULL;
    while (true)
    {
        int val1;
        cin >> val1;
        if (val1 == -1)
            break;
        insert_tail_1(head1, tail1, val1);
    }
    Node *head2 = NULL;
    Node *tail2 = NULL;
    while (true)
    {
        int val2;
        cin >> val2;
        if (val2 == -1)
            break;
        insert_tail_2(head2, tail2, val2);
    }

    if(size_1(head1)==size_2(head2))
    {
        if(node_equal(head1,head2)){
            cout<<"YES"<<endl;
        }
        else
        {
            cout<<"NO"<<endl;
        }
    }
    else if(size_1(head1)!=size_2(head2))
    {
        cout<<"NO"<<endl;
    }

    // if (node_equal(head1, head2))  
    // {
    //     cout << "YES" << endl;
    // }
    // else
    // {
    //     cout << "NO" << endl;
    // }
    return 0;
}
