#include <bits/stdc++.h>
using namespace std;
class Node
{
public:
    int val;
    Node *next;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
    }
};

void insert_at_tail(Node *&head, Node *&tail, int v)
{
    Node *newNode = new Node(v);
    if (head == NULL)
    {
        head = newNode;
        tail = newNode;
        return;
    }
    else
    {
        tail->next = newNode;
        tail = newNode;
    }
}

void difference(Node *head)
{
    Node *tmp = head;
    int mn = INT_MAX;
    int mx = INT_MIN;

    while (tmp != NULL)
    {
        mn = min(mn, tmp->val);
        mx = max(mx, tmp->val);
        tmp = tmp->next;
    }
    int result = mx - mn;
    cout << result << endl;
}

int find_difference(Node *head)
{
    Node *tmp = head;
    int mn = INT_MAX;
    int mx = INT_MIN;

    while (tmp != NULL)
    {
        mn = min(mn, tmp->val);
        mx = max(mx, tmp->val);
        tmp = tmp->next;
    }
    return mx - mn;
}

void print_linked_list(Node *head)
{
    Node *tmp = head;
    while (tmp != NULL)
    {
        cout << tmp->val << " ";
        tmp = tmp->next;
    }
    cout << endl;
}

int main()
{
    Node *head = NULL;
    Node *tail = NULL;
    while (true)
    {
        int v;
        cin >> v;
        if (v == -1)
            break;

        insert_at_tail(head, tail, v);
    }
    // print_linked_list(head);
    // int result = find_difference(head);
    // cout << result << endl;
    difference(head);

    return 0;
}
